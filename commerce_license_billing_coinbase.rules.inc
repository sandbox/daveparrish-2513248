<?php
/**
 * @file
 * Rules integration for the Commerce License Billing Coinbase module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_license_billing_coinbase_rules_condition_info() {
  $conditions['commerce_license_billing_coinbase_recurring_condition'] = array(
    'label' => t('Order is to be paid for with Coinbase recurring payment.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce License Billing Coinbase'),
    'callbacks' => array(
      'execute' => 'commerce_license_billing_coinbase_recurring_condition',
    ),
  );
  return $conditions;
}

/**
 * Rules condition callback: check if an order is a Coinbase recurring payment.
 */
function commerce_license_billing_coinbase_recurring_condition($order) {
  $is_coinbase = FALSE;
  $payment_method = '';

  if (isset($order->data['payment_method'])) {
    $payment_method = $order->data['payment_method'];
  }
  elseif (isset($order->data['first_payment_method'])) {
    $payment_method = $order->data['first_payment_method'];
  }

  $payment_method_id = explode('|', $payment_method)[0];

  if ('commerce_license_billing_coinbase' == $payment_method_id) {
    $is_coinbase = TRUE;
  }

  return $is_coinbase;
}
