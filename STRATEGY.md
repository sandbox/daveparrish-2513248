# STRATEGY

Intented to document the strategy for keeping Drupal Commerce Billing License system in sync with the Coinbase recurring payment system. Some of this document is only describing how the Commerce Billing License and Coinbase systems work and how they will work together in Commerce Billing License Coinbase.

## Normal operation

- The transaction ID is given as the 'custom' parameter when the Coinbase order is initially created
- The initial order can be found using the transaction ID.
- The license can be found by looking at the order line items, loading the line items and then loading the license from the line item data OR the billing cycle data.
- Each recurring order is created form the inital order.
- Once the Coinbase subsriber ID is determined for an inital order, it will be saved in the initial order and each recurring order to make it easier to query in the future.
- Coinbase subscriber ID is determined from an IPN or manual querying after an order is marked to be 'complete'.
- The first order transaction is marked as 'Complete' when the Coinbase returns an IPN that the order was complete. OR, if an IPN never comes in, the Drupal system will periodically query Coinbase to see if the transaction was complete.
  - If a 'recurring_order' IPN comes in, then the subscriber number will be recorded with the orders it was received for so that the order can easily check Coinbase for subscriber status.
  - If a subscriber data is not available for an order when Drupal needs to know if subscriber, then Coinbase subscribers will be queried manually.
- At the end of billing cycle Drupal checks that the subscription is active. (It would also be nice to check that the transaction went though, but I am unaware of a way to do this with only the subscriber id.)

## Exceptions

- If Drupal does not know the subscriber cannot find the subscriber for an order, then a error will occur.
- If Drupal cannot determine that an order has been paid for using Coinbase or IPN information then ???
- If an recurring_payment IPN arrives saying that the subscription has been
- cancelled or suspended then ???
