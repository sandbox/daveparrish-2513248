<?php

/**
 * @file
 * Transaction callbacks from Coinbase.
 */

/**
 * Generic information from Coinbase.
 *
 * @link https://coinbase.com/docs/merchant_tools/callbacks Callbacks @endlink
 */
function commerce_license_billing_coinbase_process_callback_generic() {
  /*
  At this point, the menu handler should have already checked the validity
  of the secret hash in the URL against the transaction. The data processed
  in this function should be considered safe if you trust the security of
  SSL on your callback URL.
  */
  watchdog('commerce_license_billing_coinbase', 'Generic Callback received.', array(), WATCHDOG_DEBUG);

  $post_body = json_decode(file_get_contents('php://input'));
  if (empty($post_body)) {
    watchdog('commerce_license_billing_coinbase', 'Generic callback was empty.', array(), WATCHDOG_NOTICE);
    return;
  }
  if (empty($post_body->recurring_payment) && empty($post_body->order)) {
    return;
  }

  // We should try to match the 'custom' parameter with the a transaction id in
  // the system and that transaction with a license.
  if (!empty($post_body->recurring_payment->custom)) {
    $transaction
      = commerce_payment_transaction_load($post_body->recurring_payment->custom);
    if (empty($transaction)) {
      watchdog('commerce_license_billing_coinbase', 'Transaction @transaction_id could not be found.',
        array('@transaction_id' => $post_body->recurring_payment->custom),
        WATCHDOG_WARNING);
      return;
    }

    $order = commerce_order_load($transaction->order_id);

    // Save the subscriber id for querying in later 'recurring_orders'.
    if ($order && empty($order->data['commerce_license_billing_coinbase']['subscriber_id'])) {
      $order->data['commerce_license_billing_coinbase']['subscriber_id']
        = $post_body->recurring_payment->id;
      commerce_order_save($order);
    }

    // TODO: Look at the status and make adjustments to the license as necessary.
    // Get specific license from line item if necessary
    // $specific_license = entity_load_single('commerce_license', XX);
    $coinbase_status
      = $post_body->recurring_payment->status;
    if (empty($coinbase_status)) {
      watchdog('commerce_license_billing_coinbase', 'Coinbase order status not found in IPN callback.',
        array(),
        WATCHDOG_WARNING);
      return;
    }

    if ($coinbase_status == 'canceled' || $coinbase_status == 'paused') {
      $licenses = commerce_license_get_order_licenses($order);
      foreach ($licenses as $license) {

        // Suspend the license.
        $license->wrapper->status->set(COMMERCE_LICENSE_REVOKED);
        $license->save();

        // set cycle status to zero and save the cycle
        $billing_cycle = commerce_license_billing_get_license_billing_cycle($license);
        $billing_cycle->status = 0;
        $billing_cycle->save();
      }
    }
    else {
      // coinbase status was not for a cancel. do nothing.
    }
  }
  else {
    // Unexpected data if this code is reached.
    watchdog('commerce_license_billing_coinbase', 'Generic callback was an unexpected type.', array(),
      WATCHDOG_WARNING);
  }
}

/**
 * Process payment information from Coinbase.
 *
 * @link https://coinbase.com/docs/merchant_tools/callbacks Callbacks @endlink
 */
function commerce_license_billing_coinbase_process_callback($order, $transaction) {
  /*
  At this point, the menu handler should have already checked the validity
  of the secret hash in the URL against the transaction. The data processed
  in this function should be considered safe if you trust the security of
  SSL on your callback URL.
  */
  watchdog('commerce_coinbase', 'Callback received for order @order_id, transaction @trans_id.', array('@trans_id' => $transaction->transaction_id, '@order_id' => $order->order_id), WATCHDOG_DEBUG, l(t('view order payments'), 'admin/commerce/orders/' . $order->order_id . '/payment'));

  $post_body = json_decode(file_get_contents('php://input'));
  if (empty($post_body)) {
    watchdog('commerce_coinbase', 'Transaction callback was empty.');
    return;
  }
  if ($post_body->order->custom != $transaction->transaction_id) {
    watchdog('commerce_coinbase', 'Possible malicious callback posted incorrect custom variable.');
    return;
  }

  if (empty($post_body->order->status)) {
    watchdog('commerce_coinbase', 'Invalid subscription callback.');
  }
  elseif ($order->status == 'checkout_payment') {
    // Update the Drupal order based on the Coinbase order.
    switch ($post_body->order->status) {
      case 'completed':
        commerce_checkout_complete($order);
        $transaction->message = t('Payment received. Confirmations pending.');
        $transaction->remote_id = $post_body->order->transaction->id;
        break;

      case 'canceled':
        $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
        $transaction->message = t('Payment was canceled.');
        break;

      default:
        watchdog('commerce_coinbase', 'Unknown payment status for transaction @id: @status', array('@id' => $transaction->transaction_id, '@status' => $post_body->order->status), WATCHDOG_WARNING);
        return;
    }
    $transaction->payload = $post_body;
    commerce_payment_transaction_save($transaction);

    // Update the Drupal transaction and order based on the Coinbase
    // transaction.
    commerce_license_billing_coinbase_update_transaction_and_order($transaction, $post_body->order->transaction->id);
  }
  else {
    // This module does not currently support the use of recurring IPN Callbacks and are ignored for now.
  }
}
